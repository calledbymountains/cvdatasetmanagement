from utils.gen_utils import *
import glob
import json
from datasets.base import BaseClass


class MSCOCO(BaseClass):
    def __init__(self, basepath, subset, year):
        super(MSCOCO, self).__init__('MSCOCO')
        if subset not in ['train', 'val']:
            raise ValueError('Subset for COCO must be one of "train" or "test"')
        self._subset = subset
        self._year = year
        self._basepath = folderexists(basepath)
        self._annpath = folderexists(os.path.join(self._basepath,
                                                  'annotations'))
        imgfolder = os.path.join(self._basepath, '{}{}'.format(subset, year))
        self._imgfolder = folderexists(imgfolder)
        self._images = glob.glob(os.path.join(self._imgfolder, '*.jpg'))
        self._instances_file = os.path.join(self._annpath,
                                            'instances_{}{}.json'.format(
                                                self._subset, self._year))
        self._captions_file = os.path.join(self._annpath,
                                           'captions_{}{}.json'.format(
                                               self._subset, self._year))
        self._keypoints_file = os.path.join(self._annpath,
                                            'person_keypoints_{}{}.json'.format(
                                                self._subset,
                                                self._year))
        self._imagemap = None
        self._catmap = None
        self._getmappings()
        self._bbox_data = self._open_annotation_file(self._instances_file)
        self._caption_data = self._open_annotation_file(self._captions_file)
        self._keypoint_data = self._open_annotation_file(self._keypoints_file)

    @staticmethod
    def _open_annotation_file(filename):
        with open(filename, 'r') as fid:
            data = json.load(fid)
        return data

    @property
    def imagemap(self):
        return self._imagemap

    @imagemap.setter
    def imagemap(self, jsondata):
        images = jsondata['images']
        imagemap = dict()
        for info in images:
            image_id = info['id']
            filename = info['file_name']
            height = float(info['height'])
            width = float(info['width'])
            imagemap[image_id] = (filename, height, width)
        self._imagemap = imagemap

    @property
    def catmap(self):
        return self._catmap

    @catmap.setter
    def catmap(self, jsondata):
        categories = jsondata['categories']
        catmap = dict()
        for info in categories:
            cat_id = info['id']
            cat_name = info['name']
            super_category = info['supercategory']
            catmap[cat_id] = (cat_name, super_category)
        self._catmap = catmap

    def _getmappings(self):
        jsondata = self._open_annotation_file(self._instances_file)
        self.imagemap = jsondata
        self.catmap = jsondata
        return None

    def _getbboxes(self):
        bbox_info = []
        for ann in self._bbox_data['annotations']:
            bbox = ann['bbox']
            height = bbox[3]
            width = bbox[2]
            xmin = bbox[0]
            ymin = bbox[1]
            xmax = xmin + bbox[2] - 1
            ymax = ymin + bbox[3] - 1
            is_crowd = ann['iscrowd']
            filename = self.imagemap[ann['image_id']][0]
            category, supercategory = self.catmap[ann['category_id']]
            segmentation = []
            if 'segmentation' in ann:
                segmentation = ann['segmentation']

            bbox_dict = dict(
                filename=filename,
                subset=self._subset,
                challenge='detection',
                xmin=xmin,
                ymin=ymin,
                xmax=xmax,
                ymax=ymax,
                is_crowd=is_crowd,
                category=category,
                supercategory=supercategory,
                segmentation=segmentation,
                height=height,
                width=width)
            bbox_info.append(bbox_dict)

        return bbox_info

    def _getcaptions(self):
        cap_info = []
        for ann in self._caption_data['annotations']:
            caption = ann['caption']
            filename = self.imagemap[ann['image_id']][0]
            cap_dict = dict(filename=filename,
                            caption=caption,
                            challenge='caption')
            cap_info.append(cap_dict)
        return cap_info

    def _getkeypoints(self):
        key_info = []
        for ann in self._keypoint_data['annotations']:
            num_keypoints = ann['num_keypoints']
            keypoints = ann['keypoints']
            filename = self.imagemap[ann['image_id']][0]
            bbox = ann['bbox']
            height = bbox[3]
            width = bbox[2]
            xmin = bbox[0]
            ymin = bbox[1]
            xmax = xmin + bbox[2] - 1
            ymax = ymin + bbox[3] - 1
            is_crowd = ann['iscrowd']
            category, supercategory = self.catmap[ann['category_id']]
            segmentation = []
            if 'segmentation' in ann:
                segmentation = ann['segmentation']
            key_dict = dict(filename=filename,
                            num_keypoints=num_keypoints,
                            challenge='keypoints',
                            keypoints=keypoints,
                            subset=self._subset,
                            xmin=xmin,
                            ymin=ymin,
                            xmax=xmax,
                            ymax=ymax,
                            is_crowd=is_crowd,
                            category=category,
                            supercategory=supercategory,
                            height=height,
                            width=width,
                            segmentation=segmentation,
                            )
            key_info.append(key_dict)
        return key_info

    def collect_annotations(self):
        bbox_info = self._getbboxes()
        cap_info = self._getcaptions()
        key_info = self._getkeypoints()
        annotations = bbox_info + cap_info + key_info
        for counter in range(len(annotations)):
            hash_id = get_hash(annotations[counter])
            annotations[counter]['_id'] = hash_id
        return annotations
