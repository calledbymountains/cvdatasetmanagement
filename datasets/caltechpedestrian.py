from utils.gen_utils import *
import glob
import numpy as np
import multiprocessing as mp
from datasets.base import BaseClass


class CaltechPedestrian(BaseClass):
    """
    Class to read and prepare the annotations of the Caltech Pedestrian dataset
    for entry into MongoDB.
    """

    def __init__(self, name, imagepath, annpath, ext='*.jpg'):
        """
        Class initializer
        :param name: Name of the dataset (e.g:- caltech10x)
        :param imagepath: Full path to the caltech images.
        :param annpath:  Full path to the caltech annotations.
        :param ext: Extension of the caltech images. Must be one of
        '*.jpg', '*.png' and '*.jpeg'.
        """
        super(CaltechPedestrian, self).__init__(name)

        self._imgpath = folderexists(imagepath, create=False)
        self._annpath = folderexists(annpath, create=False)
        if ext in ['*.jpg', '*.png', '*.jpeg']:
            self._ext = ext
        else:
            raise ValueError('The extension must be one of {}'.
                             format(list_to_str(['*.jpg', '*.png', '*.jpeg'])))

        imagelist = glob.glob(os.path.join(self._imgpath, '*'))
        annlist = glob.glob(os.path.join(self._annpath, '*'))
        imagelist = natural_sort(imagelist)
        annlist = natural_sort(annlist)
        self._imagelist = imagelist
        self._annlist = annlist

    @staticmethod
    def findset(imagefilename):
        """
        Given the full path to a caltech image, determines if it belongs to
        the training set or the testing set.
        :param imagefilename: Full path to a caltech image file.
        :return: 'train' if the image belongs to the training set.
        'test' if the image belongs to the testing set.
        """
        filename = os.path.basename(imagefilename)
        setnum = filename[3:5]
        setnum = int(setnum)
        if setnum <= 5:
            return 'train'
        else:
            return 'test'

    def collect_annotations(self):
        """
        Collects the annotations and metadata for the caltech pedestrian
        dataset.
        :return: (
                  ('bbox_full', info_full_list),
                  ('bbox_vis', info_vis_list),
                  ('metadata', metadata_list)
        where info_full_list, info_vis_list and metadata_list are python
        lists of python dictionaries.
        info_full (an element of info_full_list) and info_vis (an element of
        info_vis_list) have a similar
        structure. While info_full
        contains information for the full bounding box, info_vis contains
        information for the visible part of the bounding box.
        info_full/info_vis = dict(
        filename=<Name of the image file (not full path)>,
        category=<Name of the object category>,
        xmin=<Top-Left x-coordinate (column)>,
        ymin=<Top-Left y-coordinate (row)>,
        xmax=<Bottom-Right x-coordinate (column>,
        ymax=<Bottom-Right y-coordinate (row)>,
        height=<Height of the bounding box>,
        width =<Width of the bounding box>,
        ar=<Aspect ratio (width/height) of the bounding box>,
        occlusion=<Fraction between 0 and 1, denoting the amount of occlusion
                   in the bounding box>,
        score=<Confidence score of the bounding box in the annotations. For
              the caltech pedestrian dataset, it is always equal to 1.0>
              )

        metadata (an element of metadata_list) has the following structure:
        metadata = dict(
        filename=<Name of the image file (not full path)>,
        dataset=<Name of the dataset>,
        subset=<Subset to which the image belongs. Either 'train' or 'test'>
        )

        """
        args = []
        for imagefilename, annfilename in zip(self._imagelist, self._annlist):
            subset = self.findset(imagefilename)
            args.append((annfilename, os.path.basename(imagefilename), subset))

        pool = mp.Pool(processes=mp.cpu_count())
        results = pool.starmap(self.read_annotation, args)
        results = reduce(lambda x, y: x + y, results)
        return results

    @staticmethod
    def read_annotation(annotationfilename, imagefilename, subset):
        """
        Reads annotations from an annotation file.
        :param annotationfilename: Full path to an annotation file
        :param imagefilename: Full path to the image file.
        :param subset: Set to which the image belongs ('train' or 'test')
        :return: A tuple of 2 python lists (info_full, info_vis). Each list
        contains elements which are python dictionaries with the same structure.
        To see the details of these dictionaries, look at the doc of
        collect_annotations().
        """
        info = []
        for line in open(annotationfilename, 'r'):
            line = line.strip()
            if line[0] == '%':
                continue

            line = line.split(' ')
            category = line[0]
            bbox_full = line[1:5]
            bbox_full = list(map(float, bbox_full))
            if not all(bbox_full):
                continue
            area_full = np.prod(bbox_full[2:])
            bbox_vis = line[6:10]
            bbox_vis = list(map(float, bbox_vis))
            if not all(bbox_vis):
                bbox_vis = bbox_full
                occlusion = 0.
            else:
                area_vis = np.prod(bbox_vis[2:])
                occlusion = 1. - (area_vis / area_full)
                if occlusion < 0:
                    bbox_vis = bbox_vis
                    occlusion = 0.

            height_full = bbox_full[-1]
            height_vis = bbox_vis[-1]
            width_full = bbox_full[-2]
            width_vis = bbox_vis[-2]
            ar_full = width_full / height_full
            ar_vis = width_vis / height_vis
            dims_check = [height_full, width_full, height_vis, width_vis]
            if not all(dims_check):
                continue

            ann = dict(
                filename=imagefilename,
                category=category,
                xmin_full=bbox_full[0],
                xmax_full=bbox_full[0] + bbox_full[2],
                ymin_full=bbox_full[1],
                ymax_full=bbox_full[1] + bbox_full[3],
                height_full=height_full,
                width_full=width_full,
                ar_full=ar_full,
                xmin_vis=bbox_vis[0],
                xmax_vis=bbox_vis[0] + bbox_vis[2],
                ymin_vis=bbox_vis[1],
                ymax_vis=bbox_vis[1] + bbox_vis[3],
                height_vis=height_vis,
                width_vis=width_vis,
                ar_vis=ar_vis,
                occlusion=occlusion,
                score=1.0,
                subset=subset
            )
            hash_id = get_hash(ann)
            ann['_id'] = hash_id
            info.append(ann)

        return info
