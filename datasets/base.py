from abc import ABC, abstractmethod
from utils.gen_utils import *
from pymongo.errors import BulkWriteError


class BaseClass(ABC):
    def __init__(self, name):
        """
        Initializer for the BaseClass
        :param name: Name of the dataset
        """
        if not name:
            raise ValueError('The name of the dataset must be provided.')
        self._name = name

    @abstractmethod
    def collect_annotations(self):
        """
        An abstract method. This method should collect all the annotations of
        interest into a list of dictionaries. Each dictionary represents a
        document and the list as a whole represents a collection.
        :return: A list of python dictionaries
        """
        pass

    def write2mongo(self, hostname='localhost', port=27017):
        """
        Writes a collection of documents to MongoDB.
        :param hostname: Name of the host where the mongod server is running
        :param port: Port number.
        :return: None
        """
        info = self.collect_annotations()
        client = connect2mongo(hostname, port)
        db = getdb(client, 'CVDatasets')
        collection = db[self._name]
        try:
            collection.insert_many(info)
        except BulkWriteError:
            raise BulkWriteError('There were problems writing the documents.')
        client.close()
        return None
