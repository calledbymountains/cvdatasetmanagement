from datasets.caltechpedestrian import CaltechPedestrian
from datasets.mscoco import MSCOCO

if __name__ == "__main__":
    calped = CaltechPedestrian(
        name='caltech1x-test',
        imagepath='/data/stars/user/uujjwal/datasets/pedestrian/caltech'
                  '/caltech1x-test/images',
        annpath='/data/stars/user/uujjwal/datasets/pedestrian/caltech/caltech1x-test/annotations')

    calped.write2mongo(hostname='localhost', port=27017)

    calped = CaltechPedestrian(
        name='caltech10x-train',
        imagepath='/data/stars/user/uujjwal/datasets/pedestrian/caltech'
                  '/caltech10x-train/images',
        annpath='/data/stars/user/uujjwal/datasets/pedestrian/caltech'
                '/caltech10x-train/annotations')

    calped.write2mongo(hostname='localhost', port=27017)

    coco_basepath = '/data/stars/share/STARSDATASETS/COCO'
    year = '2017'
    COCO = MSCOCO(basepath=coco_basepath, subset='train', year=year)
    COCO.write2mongo(hostname='localhost', port=27017)

    COCO = MSCOCO(basepath=coco_basepath, subset='val', year=year)
    COCO.write2mongo(hostname='localhost', port=27017)
